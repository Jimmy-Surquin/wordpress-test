<!-- home page content -->
<section class="page-section zigzag" >
	<div class="page-section-inner">
		<h2 class="heading"><?php the_title() ?></h2>
		<div class="subheading"><?php the_excerpt() ?></div>
		<?php the_content() ?>
	</div>
</section>
<!-- ENDS home page content -->