<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'wordpress');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', '284569');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'YOveg?3=z;te##0SWnRojl!9SZgQ9!k0fT5IWX3tnKAz<6)xd.68(4Ph~5aWs KH');
define('SECURE_AUTH_KEY',  'L?VIKeThRRfK0.d@x/Gc.KskM1*~I-YnMG8^GI,F()t^B}_FO)o >L.@.5R]/tB4');
define('LOGGED_IN_KEY',    'Lxa7y-=rYa9<GTQNy%9AoHe|)#x3=56MV]a;LqJSk!9o8ERUeN(4RkQ4aAAZ3C :');
define('NONCE_KEY',        'W?Y tNV0#+:{0zC%aL#co4|:+W5.1$9UCz3/6f6@%pB)YJ-6h|@}UX0hCjwYE:0&');
define('AUTH_SALT',        ';q(;_Xu,E5X4yT-FPUQNIiNNUI2)CUKCI=tx|k6:._=563j%cXM|J-?%G:u].C=h');
define('SECURE_AUTH_SALT', '?tr<OyyVCoSV63Zmc@*zZ7{$(77T:2055W{oIV-2mqUuT_pvzY79q^wq,G~WMx.r');
define('LOGGED_IN_SALT',   '~zzWw!eS}SNn|eAT7_&Loqheo*X7!2|N7wu<n}XoCD3LPfx)YFy[!q|kCT1aE~k;');
define('NONCE_SALT',       '0LL8RM96m9+?5o${>EYe(dl_L|Y`k(O]zX=~Z|JVA`$BXV*T.%LbhnDmj0:x8Ida');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d'information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');
